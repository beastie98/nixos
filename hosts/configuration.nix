{ config, lib, pkgs, inputs, user, location, sops-nix, ... }:

let
  audioctl = pkgs.writeShellScriptBin
    "audioctl"
    ''
  #!${pkgs.bash}/bin/sh

card=$(aplay -l | awk '/card/ {print $2+0}' | sort | tail -n 1)
scontrol=$(amixer -c $card scontrols | awk -F\' '{print $2}' | head -n 1)

case $1 in
  inc)
    amixer -c $card sset $scontrol 5%+
    ;;
  dec)
    amixer -c $card sset $scontrol 5%-
    ;;
  toggle)
    amixer -c $card sset $scontrol toggle
    ;;
esac

'';
  
in
{
  imports =
    [
            ../modules/shells/fish
            ../modules/cachix

    ];

  users.users.${user} = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" "audio" "camera" "networkmanager" "lp" "scanner" "kvm" "libvirtd" "plex" ];
    initialPassword = "1234";
  };
  security.sudo.enable = false;
  security.doas = {
    enable = true;
    extraRules = [
      {
        groups = ["wheel"];
        persist = true;
      }
    ];
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    age.keyFile = "/var/lib/sops-nix/key.txt";
    age.generateKey = true;
    secrets."tum-rbg/user" = {
      sopsFile = ../secrets/tum-rbg.yaml;
    };
    secrets."tum-rbg/password" = {
      sopsFile = ../secrets/tum-rbg.yaml;
    };

  };

  time.timeZone = "Europe/Berlin";

  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  services.xserver.xkb.layout = "de,se";
  services.xserver.xkb.options = "caps:escape,grp:ctrls_toggle";
  services.libinput = {
    enable = true;
    mouse.naturalScrolling = true;
  };
  services.openssh.enable = true;

  fonts.packages= with pkgs; [
    carlito
    vegur
    mononoki
    (iosevka.override {
        privateBuildPlan = builtins.readFile ./iosevka.toml;
        set = "Custom";
      })
    source-code-pro
    jetbrains-mono
    font-awesome
    libertine
    libertinus
    nil
    etBook
    libre-baskerville
    liberation_ttf
    inter
    noto-fonts
    (nerdfonts.override {
      fonts = [
        "Go-Mono"
        "SourceCodePro"
        "Mononoki"
      ];
    })
  ];

  fonts.fontconfig = {
    defaultFonts = {
      monospace = [ "Iosevka Custom Md"];
      serif = [ "libre-baskerville" "Noto Nastaliq Urdu" ];
      sansSerif = [ "inter" "Noto Nastaliq Urdu" ];
    };
  };  


  environment = {
    variables = {
      TERMINAL = "foot";
      EDITOR = "nvim";
      VISUAL = "nvim";
      XKB_DEFAULT_LAYOUT = "de,se";
      XKB_DEFAULT_OPTIONS = "caps:escape,grp:ctrls_toggle";
      WLR_NO_HARDWARE_CURSORS = "1";
    };
    systemPackages = with pkgs; [
      xorg.setxkbmap
      age
      comma
      neovim
      nixfmt-rfc-style
      git
      killall
      nano
      pciutils
      usbutils
      wget
      gnupg
      pinentry
      light
      nix-index
      alsa-utils
      audioctl
      ripgrep
      htop
      openvpn
      texlive.combined.scheme-full
      curlftpfs
      cachix
      (agda.withPackages (p: [ p.standard-library p.cubical ]))
      zip
      unzip
    ];
  };

  security.rtkit.enable = true;
  services = {
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    dbus.enable = true;
  };

  environment.etc = {
    "asound.conf".text = ''
defaults.pcm.card 1
defaults.ctl.card 1
'';
  };

  xdg.portal = {
    enable = true;
    wlr.enable=true;
    #extraPortals = [ pkgs.xdg-desktop-portal-gtk pkgs.xdg-desktop-portal-wlr];
    extraPortals = [pkgs.xdg-desktop-portal-wlr];
  };

  nixpkgs.config.permittedInsecurePackages = [
    "olm-3.2.16"
  ];
  nix = {
    settings ={
      sandbox = true;
      auto-optimise-store = true;
      substituters = [
        "https://nix-community.cachix.org"
        "https://cache.nixos.org/"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
    registry.nixpkgs.flake = inputs.nixpkgs;
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs          = true
      keep-derivations      = true
    '';
  };
  nixpkgs.config.allowUnfree = true;

  system = {
    autoUpgrade = {
      enable = true;
      channel = "https://nixos.org/channels/nixos-unstable";
    };
    stateVersion = "24.05";
  };

#networking.wifi.enable=true;
networking.networkmanager = {
    enable = true;
    plugins = [ pkgs.networkmanager-openvpn ];
};

networking.networkmanager.ensureProfiles.profiles = {
  "38C3" = {
    connection = {
      id = "38C3";
      type = "wifi";
    };
    wifi = {
      mode = "infrastructure";
      ssid = "38C3";
    };
    wifi-security = {
      auth-alg = "open";
      key-mgmt = "wpa-eap";
    };
    "802-1x" = {
      anonymous-identity = "38C3";
      eap = "ttls;";
      identity = "38C3";
      password = "38C3";
      phase2-auth = "pap";
      altsubject-matches = "DNS:radius.c3noc.net";
      ca-cert = "${builtins.fetchurl {
        url = "https://letsencrypt.org/certs/isrgrootx1.pem";
        sha256 = "sha256:1la36n2f31j9s03v847ig6ny9lr875q3g7smnq33dcsmf2i5gd92";
      }}";
    };
    ipv4 = {
      method = "auto";
    };
    ipv6 = {
      addr-gen-mode = "default";
      method = "auto";
    };
  };
};

networking.dhcpcd.extraConfig = "nohook resolv.conf";
networking.nameservers = [ "8.8.8.8" "1.1.1.1" ];
services.openvpn.servers = {
  il7 = {
    config = '' config /home/vincent/openvpn/vpn-il7-standard.ovpn '';
    autoStart = false;
  };
};
#  networking.wireless.networks."37C3".auth = ''
#  key_mgmt=WPA-EAP
#  eap=TTLS
#  identity="37C3"
#  password="37C3"
#  ca_cert="${builtins.fetchurl {
#    url = "https://letsencrypt.org/certs/isrgrootx1.pem";
#    sha256 = "sha256:1la36n2f31j9s03v847ig6ny9lr875q3g7smnq33dcsmf2i5gd92";
#  }}"
#  altsubject_match="DNS:radius.c3noc.net"
#  phase2="auth=PAP"
#'';
}
