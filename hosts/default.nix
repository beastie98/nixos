{ lib, inputs, nixpkgs, nixpkgs-unstable, home-manager, emacs-overlay, user, location, sops-nix, ... }:

let
  system = "x86_64-linux";

  pkgs = import nixpkgs {
    inherit system;
  };

  lib = nixpkgs.lib;
in
{
  pegasus = lib.nixosSystem {
    inherit system;
    specialArgs = { inherit inputs user location emacs-overlay; };
    modules = [
      ({config, pkgs, ...} :
        let unstable-overlay = final: prev: {
              unstable = nixpkgs-unstable.legacyPackages.x86_64-linux;
            };
            geiser-overlay = final: prev: {
                  emacs-geiser = prev.emacs-geiser.overrideAttrs (old : {
                    src = prev.fetchFromGitlab {
                      owner = "emacs-geiser";
                      repo = "geiser";
                      rev = "32196db8f8ddab071565a5ae6d799ada4f8fbe6b";
                      sha256 = "13r87v4aqrdsk62jwp0ywnl0ih51fvgskygg9r6pr9cxswddfaim";
                    };
                  });
                };
        in
          {
            nixpkgs.overlays = [
              unstable-overlay
              geiser-overlay
              emacs-overlay.overlay
              (import ../modules/epkowa/overlay.nix)
              (import ../modules/isabelle/overlay.nix)
            ];
          })


      ./pegasus
      ./configuration.nix
      sops-nix.nixosModules.sops

      home-manager.nixosModules.home-manager {
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
        home-manager.extraSpecialArgs = { inherit user; }; 
        home-manager.users.${user} = {
          imports = [(import ./home.nix)] ++ [(import ./pegasus/home.nix)];
        };
      }

    ];
  };
  monoceros = lib.nixosSystem {
    inherit system;
    specialArgs = { inherit inputs user location emacs-overlay; };
    modules = [
      ({config, pkgs, ...} :
        let unstable-overlay = final: prev: {
              unstable = nixpkgs-unstable.legacyPackages.x86_64-linux;
            };
        in
          {
            nixpkgs.overlays = [
              unstable-overlay
          emacs-overlay.overlay
          (import ../modules/epkowa/overlay.nix)
          (import ../modules/isabelle/overlay.nix)
        ];
      })


      ./monoceros
      ./configuration.nix

      home-manager.nixosModules.home-manager {
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
        home-manager.extraSpecialArgs = { inherit user; }; 
        home-manager.users.${user} = {
          imports = [(import ./home.nix)] ++ [(import ./pegasus/home.nix)];
        };
      }

    ];
  };

}
