{ config, lib, pkgs, user, ... }:

{ 

  imports = [
  ];

  home = {
    username = "${user}";
    homeDirectory = "/home/${user}";
    stateVersion = "24.05";
  };

  programs = {

    direnv.enable = true;
    direnv.nix-direnv.enable = true;
    fish = {
      enable = true;
    interactiveShellInit = ''
direnv hook fish | source
'';
    };

    home-manager.enable = true;
    fzf = {
      enable = true;
    };
    git = {
      enable = true;
      userName = "Vincent Fischer";
      userEmail = "fischervincent98@gmail.com";
      signing.signByDefault = true;
      signing.key = null;
    };
  };
}
