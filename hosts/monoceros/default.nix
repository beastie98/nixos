{ config, pkgs, ... }:

{
  imports =  [
    ./hardware-configuration.nix
    ../../modules/desktop/river
    ../../modules/emacs
    ../../modules/helix
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_xanmod_latest;

    loader = {
      systemd-boot.enable = true;
      
    };
  };
  programs.dconf.enable = true;
  programs.light.enable = true;
  networking.hostName = "monoceros";
  networking.domain = "astrobeastie.net";
  networking.wg-quick.interfaces = {
   wg0 = {
    address= ["fdef:5d0d:ee12:01ff::666/64"];
    privateKey = "mAwemN7Nmlu6Kifsah4YoFZxFdNAZYBj+e2TmHzzmUc=";
    
    peers = [
      {
        publicKey = "IYxvt/vqh/chOdWBm5auU+PRsHh9qL/WcNWerV8/0ys=";
        endpoint = "vpn.gate0.rtr.defra0.airmass1.net:41100";
        allowedIPs = [ "fd00::/8" "fe80::/10 "];
      }
    ];
    
  }; 
    
  };

  services.gvfs.enable = true;
  services.udisks2.enable = true;
  services.devmon.enable = true;
  services.locate.enable = true;
  environment.systemPackages = with pkgs; [
  wireguard-tools
    epkowa
    xsane
    linuxKernel.packages.linux_xanmod_latest.perf
  ];

  services.printing.enable = true;
  services.printing.drivers = [pkgs.hplipWithPlugin pkgs.hplip];
  
  hardware.sane = {
    enable = true;
    extraBackends = [ pkgs.epkowa pkgs.hplipWithPlugin ];
  };
  services.udev.packages = [ pkgs.epkowa pkgs.hplipWithPlugin ];
  services.udev.extraRules = ''
ATTRS{manufacturer}=="EPSON", DRIVERS=="usb", SUBSYSTEMS=="usb", ATTRS{idVendor}=="04b8", ATTRS{idProduct}=="*", MODE="0777"

ATTRS{idVendor}=="239a", ENV{ID_MM_DEVICE_IGNORE}="1"
SUBSYSTEM=="usb", ATTRS{idVendor}=="239a", MODE="0666"
SUBSYSTEM=="tty", ATTRS{idVendor}=="239a", MODE="0666"

SUBSYSTEM="usb", ATTR{idVendor}=="03f0", ATTR{idProduct}=="bf2a", MODE="0777"
'';

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;
  environment.etc = {
    "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
    bluez_monitor.properties = {
      ["bluez5.enable-sbc-xq"] = true,
      ["bluez5.enable-msbc"] = true,
      ["bluez5.enable-hw-volume"] = true,
      ["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
                                                }
  '';
  };

#hardware.opentabletdriver.enable = true;
#programs.hyprland.enable = true;
services.xserver.wacom.enable = true;  

#hardware.opentabletdriver.enable = true;
#hardware.opentabletdriver.daemon.enable = true;
#virtualisation.waydroid.enable = true;
}
