{lib, pkgs, config, ... }:
let
  inherit (lib) importJSON;
  factorio_cfg = import ./factorio.nix;  
  in
{

  
  imports =
    [
      ../../modules/desktop/river/home.nix
      ../../modules/emacs/home.nix
      ../../modules/helix/home.nix
    ];
    

  home = {
    packages = with pkgs; [
      mumble
      (waybar.overrideAttrs (oldAttrs: {
        mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true"];
      }))
      eduvpn-client
      mupen64plus
      wl-mirror
      neochat
      nheko
      tenacity
      keepassxc
      firefox-wayland
      chromium
      thunderbird
      stellarium
      paraview
      mu
      libreoffice
      darktable
      rawtherapee
      gimp
      krita
      siril
      inkscape
      isabelle
      coq
      freecad
      file
      imagemagick
      unzip
      superTux
      dosbox
      mplayer
      xournalpp
      sway
      zulip
      xorg.libSM
    ];
  };

  gtk = {
    enable = true;
    theme = {
      name = "arc-dark";
      package = pkgs.arc-theme;
    };
  };

  xdg.enable = true;
  xdg.mimeApps = {
    enable = true;
    defaultApplications = {
      "inode/directory" = "pcmanfm.desktop";
      "text/plain" = "emacsclient.desktop";
      "text/english" = "emacsclient.desktop";
      "text/x-makefile" = "emacsclient.desktop";
      "text/x-c++hdr" = "emacsclient.desktop";
      "text/x-c++src" = "emacsclient.desktop";
      "text/x-chdr" = "emacsclient.desktop";
      "text/x-csrc" = "emacsclient.desktop";
      "text/x-java" = "emacsclient.desktop";
      "text/x-moc" = "emacsclient.desktop";
      "text/x-pascal" = "emacsclient.desktop";
      "text/x-tcl" = "emacsclient.desktop";
      "text/x-tex" = "emacsclient.desktop";
      "application/x-shellscript" = "emacsclient.desktop";
      "text/x-c" = "emacsclient.desktop";
      "x-scheme-handler/org-protocol" = "emacsclient.desktop";
      "text/x-csv" = "emacsclient.desktop";
      "text/x-rust" = "emacsclient.desktop";
      "x-scheme-handler/http" = "firefox.desktop";
      "x-scheme-handler/https" = "firefox.desktop";
      "x-scheme-handler/chrome" = "firefox.desktop";
      "text/html" = "firefox.desktop";
      "application/x-extension-htm" = "firefox.desktop";
      "application/x-extension-html" = "firefox.desktop";
      "application/x-extension-shtml" = "firefox.desktop";
      "application/xhtml+xml" = "firefox.desktop";
      "application/x-extension-xhtml" = "firefox.desktop";
      "application/x-extension-xht" = "firefox.desktop";
    };
  };
  xdg.userDirs.enable = true;
  xdg.userDirs.createDirectories = true;
}
