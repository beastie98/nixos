{
  description = "My Personal NixOS System Flake Configuration";

  inputs =
    rec {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
      nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

      home-manager = {
        url = "github:nix-community/home-manager/release-24.11";
        inputs.nixpkgs.follows = "nixpkgs";
      };

      sops-nix = {
        url = "github:Mic92/sops-nix";
        inputs.nixpkgs.follows = "nixpkgs";
      };

      emacs-overlay = {
        #url = "github:nix-community/emacs-overlay";
        url = "github:nix-community/emacs-overlay/";
        inputs.nixpkgs.follows = "nixpkgs-unstable";
      };
      
     };

  outputs = inputs @ { self, nixpkgs, nixpkgs-unstable, home-manager, emacs-overlay, sops-nix, ... }:
    let
      user = "vincent";
      location = "$HOME/.setup";
    in
    {
      nixosConfigurations = (
        import ./hosts {
          inherit (nixpkgs) lib;
          inherit inputs nixpkgs nixpkgs-unstable home-manager emacs-overlay sops-nix user location ;
        }
      );
    };
}
