#!/bin/sh

card=$(aplay -l | awk '/card/ {print $2+0}' | sort | tail -n 1)
scontrol=$(amixer -c $card scontrols | awk -F\' '{print $2}' | head -n 1)
volume=$(amixer -c $card sget $scontrol | awk -F\[ '/  [^(Limit)].*: Playback/ {total +=  $2+0; count++} END {print total/count}')
amixer -c $card sget $scontrol | awk -F\[ '/  [^(Limit)].*: Playback/ {print $4}' | grep -q on
muted=$?


echo "volume|int|$volume"
if [ $muted -eq 1 ]
then
		echo "muted|bool|true"
else
		echo "muted|bool|false"
fi
echo 

