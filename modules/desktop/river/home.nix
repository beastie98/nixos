{ config, nixosConfig, lib, pkgs, protocol, ... }:

{
  home.packages = with pkgs; [
    imv
    pcmanfm
    pinentry-gtk2
    zathura
  ];

  home.file.".config/river" = {
    source = ./config/river;
    recursive = true;
  };
  
  home.file.".config/yambar" = {
    source = ./config/yambar;
    recursive = true;
  };
    
  home.file.".config/swaync" = {
    source = ./config/swaync;
    recursive = true;
  };

  home.file.".config/foot/foot.ini".source = ./config/foot/foot.ini;

  home.file.".config/dt.png".source = ./config/dt.png;
  
}
