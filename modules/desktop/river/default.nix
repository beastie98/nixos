{ config, lib, pkgs, protocol, user, ... }:

let

  start-river = pkgs.writeTextFile {
    name = "start-river";
    destination = "/bin/start-river";
    executable = true;
    text = ''
#!/bin/sh
dbus-run-session river
'';
  };

  dbus-river-environment = pkgs.writeTextFile {
    name = "dbus-river-environment";
    destination = "/bin/dbus-river-environment";
    executable = true;

    text = ''
  dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river
  systemctl --user import-eenvironment DBUS_SESSION_BUS_ADDRESS DISPLAY SSH_AUTH_SOCK XAUTHORITY XDG_DATA_DIRS XDG_RUNTIME_DIR XDG_SESSION
_ID
  systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
  systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      '';
  };

  # currently, there is some friction between sway and gtk:
  # https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
  # the suggested way to set gtk settings is with gsettings
  # for gsettings to work, we need to tell it where the schemas are
  # using the XDG_DATA_DIR environment variable
  # run at the end of sway config
  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
        export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
        gnome_schema=org.gnome.desktop.interface
        gsettings set $gnome_schema gtk-theme 'Dracula'
        '';
  };


in

{
  hardware.graphics.enable = true;

  environment.systemPackages = with pkgs; [
    cage
    (river.overrideAttrs (prevAttrs: rec {
      postInstall =
        let
          riverSession = ''
            [Desktop Entry]
            Name=River
            Comment=Dynamic Wayland compositor
            Exec=river
            Type=Application
          '';
        in
          ''
          mkdir -p $out/share/wayland-sessions
          echo "${riverSession}" > $out/share/wayland-sessions/river.desktop
        '';
      passthru.providedSessions = [ "river" ];
    }))
    foot
    (yambar.overrideAttrs (prevAttrs: rec {
      src = fetchFromGitea {
        domain = "codeberg.org";
        owner = "dnkl";
        repo = "yambar";
        rev = "739dc30323adbb93cfad1918eca4bb0f18b41359";
        hash = "sha256-YZZ0VMqitXj74aPlEolhVnmXDPwogWSFRZiylcy3YfI=";
      };
    }))
    swaybg
    fuzzel
    dbus-river-environment
    start-river
    configure-gtk
    wayland
    glib
    dracula-theme
    adwaita-icon-theme
    swaylock
    swayidle
    grim
    wl-clipboard
    swaynotificationcenter
    wlr-randr
    libsForQt5.qtstyleplugin-kvantum
  ];

  security.pam.services.swaylock = {};

  services.displayManager.sessionPackages = [
    (pkgs.river.overrideAttrs (prevAttrs: rec {
      postInstall =
        let
          riverSession = ''
            [Desktop Entry]
            Name=River
            Comment=Dynamic Wayland compositor
            Exec=river
            Type=Application
          '';
        in
          ''
          mkdir -p $out/share/wayland-sessions
          echo "${riverSession}" > $out/share/wayland-sessions/river.desktop
        '';
      passthru.providedSessions = [ "river" ];
    }))
  ];

  environment.variables = {
    "QT_STYLE_OVERRIDE"="kvantum";
  };

  programs.regreet.enable=true;
  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        command = "${pkgs.cage}/bin/cage -s -- ${pkgs.greetd.regreet}/bin/regreet";
        user = "greeter";
      };
    };
  };

  environment.etc."greetd/environments".text = ''
dbus-run-session river
'';
  
}
