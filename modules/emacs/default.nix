{config, pkgs, ...}:

{
  environment.systemPackages = [
    (pkgs.emacsWithPackagesFromUsePackage {

      config = ./emacs.d/conf.org;
      package = pkgs.emacs-pgtk;
      alwaysEnsure = true;
      alwaysTangle = true;
    })
  ];
}
