{pkgs, lib }:

with builtins;

{
  isabelle_home = "${pkgs.isabelle}/Isabelle${pkgs.isabelle.version}";
}
