{ config, nixosConfig, lib, pkgs, protocol, ... }:

{

  services.emacs.package = (pkgs.emacsWithPackagesFromUsePackage {
    config = ./emacs.d/conf.org;
    package = pkgs.emacs-pgtk;
    alwaysEnsure = true;
    alwaysTangle = true;
  });
  services.emacs.enable = true;

  home.file.".emacs.d/init.el".source = ./emacs.d/init.el;
  home.file.".emacs.d/conf.org".source = pkgs.substituteAll ((import ./substitutions.nix { inherit lib; inherit pkgs; })
                                                             // {src = ./emacs.d/conf.org;});
  home.file.".emacs.d/conf.org".onChange="rm -rf ~/.emacs.d/conf.el";
  home.file.".emacs.d/lsp-isar" = {
    source = ./emacs.d/lsp-isar;
    recursive = true;
  };
  #home.file.".emacs.d/org-cv" = {
   # source = ./emacs.d/org-cv;
   # recursive = true;
  #};
  home.sessionVariables = {
    EDITOR = "emacslient -nw";
  };

}
