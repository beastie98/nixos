{ config, lib, pkgs, protocol, ... }:
{
  programs.fish = {
    enable = true;
    
    shellInit = ''

# Gruvbox Color Palette
set -l foreground ebdbb2
set -l selection 282828 
set -l comment 928374 
set -l red fb4934
set -l orange fe8019
set -l yellow fabd2f
set -l green b8bb26
set -l cyan 8ec07c
set -l blue 83a598
set -l purple d3869b

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $blue
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $blue
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment

'';
    promptInit = ''

function fish_prompt
    set -l last_status $status

    set -l normal (set_color normal)
    set -l green (set_color green)
    set -l red (set_color red)
    set -l yellow (set_color yellow)
    set -l blue (set_color blue)
    set -l purple (set_color magenta)
    set -l aqua (set_color cyan)

    set -g __fish_git_prompt_char_stateseparator ' '
    set -g __fish_git_prompt_color cyan
    set -g __fish_git_prompt_color_branch green
    set -g __fish_git_prompt_color_branch_dirty yellow
    set -g __fish_git_prompt_color_branch_staged yellow
    set -g __fish_git_prompt_color_branch_detached red
    set -g __fish_git_prompt_color_branch_flags yellow
    set -g __fish_git_prompt_color_merging red
    set -g __fish_git_prompt_showcolorhints true
    set -g __fish_git_prompt_showdirtystate true


    if [ (id -u) -eq 0 ]
        if [ "$SSH_TTY" = "" ]
            echo -n ┌┤$red$USER$aqua@$green(prompt_hostname)$normal
        else
            echo -n ┌┤$red$USER$aqua@$red(prompt_hostname)$normal
        end
        echo -n ' '
    else
        if [ "$SSH_TTY" = "" ]
            echo -n ┌┤$green$USER$aqua@$green(prompt_hostname)$normal
        else
            echo -n ┌┤$green$USER$aqua@$red(prompt_hostname)$normal
        end
        echo -n ' '
    end

    set -l git (fish_git_prompt)
    echo -n "$blue"(pwd)"$git$normal├"
    set -l dummyprompt "┌┤$USER@"(prompt_hostname)" "(pwd)"$git├┤"(date +%d-%m-%y\ %H:%M:%S)"├"
    set -l dummyprompt (echo $dummyprompt | perl -pe 's/\x1b.*?[mGKH]//g')
    set -l columns (math $COLUMNS - (string length -- "$dummyprompt"))
    set line ""
    for i in (seq 1 $columns)
        set line "─"$line
    end
    echo -n $line
    echo -n "┤$purple"(date +%d-%m-%y\ %H:%M:%S)"$normal├"
    echo
    echo -n $normal"└"
    if [ $last_status -ne 0 ]
        echo -n "┤"(set_color -b red)"$last_status"(set_color -b normal)"├"
    end
    if [ (id -u) -eq 0 ]
       echo -n $red" λ "
    else
       echo -n $green" λ "
    end
end
'';

    shellAbbrs = {
      e = "emacsclient -nw";
      sudo = "doas";
    };
  };

  users.defaultUserShell = pkgs.fish;
}
