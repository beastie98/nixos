self: super:
{
  isabelle = super.isabelle.overrideAttrs (old: 
    let
      src_dev = super.fetchFromGitHub {
        owner = "m-fleury";
        repo = "isabelle-emacs";
        rev = "Isabelle2024-vsce";
        sha256 = "sha256-MAIq5dQ71QBZQbwGdNE+jS+I9sm/dbrK1xacLEFYobI=";
      };
    in {
      prePatch = ''
            rm -r src/
            cp -r ${src_dev}/src ./
            cp ${src_dev}/etc/build.props etc/
            chmod -R +w ./src
         '';
    }
  );
}
