{ config, nixosConfig, lib, pkgs, protocol, ... }:

{
  home.file.".config/helix/" = {
    source = ./helix;
    recursive = true;
  };
}
