{config, pkgs, ...}:

{
  environment.systemPackages = [
    pkgs.helix
  ];
}
